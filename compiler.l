%{

#include "..\compiler.h"
#include "..\symboltable.h"
#include "compiler.tab.h"

/**
 * Variable to track current line number.
 */
int inputLineNumber = 1;

%}

%option nounput

%%



int {return INT;}

bool {return BOOL;}

float {return FLOAT;}

true|false {return BOOLEANS;}

and {return AND;}

or {return OR;}

not {return NOT;}

 


\= {return REFER;}

\* {return TIMES;}

\+ {return PLUS;}

\/ {return DIVIDE;}

\- {return MINUS;}

\< {return SMALLER;}

\> {return BIGGER;}

\<\= {return EQLESS;}

\>\= {return EQHIGH;}

\++ {return INC;}

\-- {return DEC;}

\!= {return UNEQUAL;}

\== {return EQUALS;}

% {return MODULO;}



if {return IF;}

then {return THEN;}

else {return ELSE;}

while {return WHILE;}

do {return DO;}

end {return END;}

exit {return EXIT;}



[ ]+ {}

#.* ;

; {return SEMI;}

, {return COMMA;}

\( {return OPEN;}

\) {return CLOSE;}

[\n]+ ++inputLineNumber;

[\t]+ ;

[A-Za-z][a-zA-Z0-9]* {
yylval.str = strdup(yytext);
return NAME;}

\-?[0-9][0-9]* {
yylval.integer = atoi(yytext);
return DIGIT;}

\-?[0-9]+\.[0-9]+ {
yylval.real = atof(yytext);
return FLOATDIGIT;}

. {yylval.str = strdup(yytext); printf("Unexpected Symbol: '%s' in line %d\n", yylval.str, inputLineNumber);}


%%
