/**
 * @file compiler.c
 * @author Ramon Bisswanger
 * @version 1.1 (April 2015)
 * @brief This contains all general function implementations for the compiler.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "compiler.h"
#include "symboltable.h"

int counter = 0;

/**
 * Variable to track current line number.<BR>
 * [defined in file compiler.l]
 */
extern int inputLineNumber;

/**
 * Variable to enable/disable debug mode<BR>
 * Set to a value unequal to <code>0</code> to enable debug output
 */
int debug = 0;

/**
 * Main application entry point.<BR>
 * Uses input from STDIN and forwards it to the scanner for processing.
 */
int main(void)
{
    yyparse();
    return 0;
}

/**
 * This function is called by the parser if an error has been detected while
 * parsing the input data (e.g. syntax error).
 * @param str The error message to be printed.
 */
void yyerror(char* str)
{
    fprintf(stderr, "Error while parsing input file (Line: %d): %s\n",
            inputLineNumber, str);
}

char* helpCount() 
{
	char buffer[30] = {};
	itoa(counter, buffer, 10);
	counter++;
	strcat(buffer, "helpVar");
	return strdup(buffer);
}
