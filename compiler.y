%{
//Prologue

#include "..\compiler.h"
#include "..\symboltable.h"
#include "..\generator.h"
#include "..\interpreter.h"
#include <stdio.h>

// Variable to track current line number  [defined in compiler.l]
extern int inputLineNumber;
extern char* helpCount();

%}
//Bison declarations

%error-verbose
%start S

%union{
int integer;
float real;
int boolean;
dataType varType;
symbolTableEntry* symbolEntry;
char* str;
}

// Define all tokens used in the scanner
%token INT BOOL FLOAT
%token NAME DIGIT FLOATDIGIT BOOLEANS
%token REFER TIMES PLUS DIVIDE MINUS SMALLER BIGGER EQUALS EQLESS EQHIGH INC DEC UNEQUAL MODULO
%token AND OR NOT
%token IF ELSE WHILE THEN DO END EXIT
%token SEMI COMMA OPEN CLOSE
%token BREAK UNKNOWN


//Types
%type<integer> INT
%type<float> FLOAT
%type<boolean> BOOL
%type<str> NAME
%type<varType> VARS
%type<varType> ALLOCATE
%type<varType> EXP
%type<varType> CALC
%type<varType> EXPRESSIONS


//Prios
%left OR
%left AND
%left NOT
%left SMALLER BIGGER EQUALS EQLESS EQHIGH UNEQUAL
%left PLUS MINUS
%left TIMES DIVIDE MODULO

%right INC DEC

%%
// grammar rules

S: PROGRAM EXIT EXP SEMI

PROGRAM: CLAUSE SEMI
    |PROGRAM CLAUSE SEMI

CLAUSE: ALLOC
    |DEFINE
    |IFCLAUSE
    |WHILELOOP
    |EXP
    |
    
//Alloc Zuweisung
ALLOC: NAME REFER EXP 
{
	symbolTableEntry* p1 =getEntryFromSymbolTable($1);
	if(p1 == 0)
	{
		printf("Error: undeclared variable %d\n", inputLineNumber);
		YYABORT;
	}
	else if(p1->type != $3)
	{
		printf("Error: typeconflict in %d\n", inputLineNumber);
		YYABORT;
	}
	// printf("Error: %d \n", $3);
		
}

//Define
DEFINE: 
	INT INTASSIGN
	|BOOL BOOLASSIGN
	|FLOAT FLOATASSIGN

INTASSIGN: NAMEINTTMP ALLOCATE INTREST {
	if($2 != INTEGER && $2 != 5)
	{
		printf("type confict in line %d \n", inputLineNumber);
	}
	}

NAMEINTTMP:
	NAME
	{
		if(getEntryFromSymbolTable($1) != 0){
			printf("Error: Redeclared Variable\n");
		}
		addEntryToSymbolTable($1, INTEGER,inputLineNumber);
	}
	
	
BOOLASSIGN: NAMEBOOLTMP ALLOCATE BOOLREST {
	if($2 != BOOLEAN && $2 != 5)
	{
		printf("type confict in line %d\n", inputLineNumber);
	}
	}

NAMEBOOLTMP:
	NAME
	{
		if(getEntryFromSymbolTable($1) != 0){
				printf("Error: Redeclared Variable %d\n", inputLineNumber);
		}
	addEntryToSymbolTable($1, BOOLEAN,inputLineNumber);
	}
	
FLOATASSIGN: NAMEFLOATTMP ALLOCATE FLOATREST {
	if($2 != REAL && $2 != 5)
	{
		printf("type confict in line %d\n", inputLineNumber);
	}
	}
	
NAMEFLOATTMP:
	NAME
	{
	if(getEntryFromSymbolTable($1) != 0){
		printf("Error: Redeclared Variable\n");
	}
	addEntryToSymbolTable($1, REAL,inputLineNumber);
	}

ALLOCATE: REFER EXP {$$ = $2;}
    |  { $$ = 5; }
    
INTREST: COMMA INTASSIGN 
    |
	
BOOLREST: COMMA BOOLASSIGN
	|
	
FLOATREST: COMMA FLOATASSIGN
	| 

//x
//IFCLAUSE
IFCLAUSE: IF OPEN EXP CLOSE THEN X Y END
{
	if($3 != BOOLEAN)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
} 

X: EXP SEMI X
    |ALLOC SEMI X
    |IFCLAUSE SEMI X
    |WHILELOOP SEMI X
    | 
    
Y:ELSE X Y
    | 
//x  
//WHILELOOP
WHILELOOP: WHILE OPEN EXP CLOSE DO X END
{
	if($3 != BOOLEAN)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
} 

//Ausdrücke

VARS: NAME 
{ 
	symbolTableEntry* p1 = getEntryFromSymbolTable($1);
	if( p1 == 0)
	{
		printf("Error: undeclared variable %d\n", inputLineNumber);
		YYABORT;
	} else {
	$$ = p1->type; 
	}
}
    | DIGIT {$$ = INTEGER;}
    | FLOATDIGIT {$$ = REAL;}
    
EXP: OPEN EXPRESSIONS CLOSE {$$ = $2;}
    |EXPRESSIONS {$$ = $1;}

	
EXPRESSIONS: VARS {$$ = $1;}
    | BOOLEANS 
	{
		 $$ = BOOLEAN;
		// addEntryToSymbolTable($$, BOOLEAN ,inputLineNumber);
	}

    | INC VARS 
	{
	if($2 != INTEGER)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}

		$$ = $2;
	}

    | DEC VARS 
	{
		if($2 != INTEGER)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
		$$ = $2;
	}

	| CALC
	{
		$$ = $1;
	}
	
	
CALC:
	EXP PLUS EXP
	{
		if($1 != $3)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
		$$ = $1;
	}
	| EXP MINUS EXP
	{
		if($1 != $3)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
		$$ = $1;	
	}
	| EXP TIMES EXP
	{
		if($1 != $3)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
		$$ = $1;	
	}
	| EXP DIVIDE EXP
	{
		if($1 != $3)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
		$$ = $1;	
	}
	| EXP MODULO EXP
	{
		if($1 != INTEGER || $3 != INTEGER)
	{
		printf("Typeconflict in %d", inputLineNumber);
	}
		$$ = $1;		
	}
	| EXP SMALLER EXP
	{
		$$ = BOOLEAN;		
	}
	| EXP BIGGER EXP
	{
		if($1 == BOOLEAN || $3 == BOOLEAN)
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;	
	}
	| EXP EQUALS EXP
	{
		if($1 == BOOLEAN || $3 == BOOLEAN)
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;	
	}
	| EXP EQLESS EXP
	{
		if($1 == BOOLEAN || $3 == BOOLEAN)
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;	
	}
	| EXP EQHIGH EXP
	{
		if($1 == BOOLEAN || $3 == BOOLEAN)
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;	
	}
	| EXP UNEQUAL EXP
	{
		if($1 != $3) 
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;	
	}
	
    | EXP AND EXP
	{
		if($1 != $3) 
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;
	}

    | EXP OR EXP
	{
		if($1 != $3) 
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;
	}
	| NOT EXP
	{
		if($2 != BOOLEAN) 
			printf("Typeconflict in %d", inputLineNumber);
		$$ = BOOLEAN;
	}

%%

//Epilogue